(defpackage cl-rogue
  (:use :cl)
  (:export #:main))
(in-package :cl-rogue)

;;;; global state  --------------------------------------------------------------

(defparameter *running* nil)


;;;; scenes ---------------------------------------------------------------------

(defclass scene () ())

(defgeneric display-scene (scene window))

(defgeneric handle-input (scene key))


(defclass start-scene (scene) ())

(defmethod display-scene ((s start-scene) window)
  (draw-string window 0 2 "        CORYS ROGUELIKE")
  (draw-string window 0 4 "    Press [Space] to start")
  (draw-string window 0 5 "       Press [Q] to quit"))

(defmethod handle-input ((s start-scene) key)
  (case key
    (#\Space (make-instance 'play-scene))
    (#\Q (progn
	   (setf *running* nil)
	   s))
    (otherwise s)))

(defclass play-scene (scene) ())

(defmethod display-scene ((s play-scene) window)
  (draw-string window 0 0 "          You are having fun.")
  (draw-string window 0 1 "-- press [Esc] to lose or [Enter] to win --"))

(defmethod handle-input ((s play-scene) key)
  (case key
    (#\Escape (make-instance 'lose-scene))
    (#\Newline (make-instance 'win-scene))
    (otherwise s)))


(defclass win-scene (scene) ())

(defmethod display-scene ((s win-scene) window)
  (draw-string window 0 0 "         !! YOU WIN !!")
  (draw-string window 0 1 "-- press [Enter] to restart --"))

(defmethod handle-input ((s win-scene) key)
    (case key
      (#\Newline (make-instance 'start-scene))
      (otherwise s)))

(defclass lose-scene (scene) ())

(defmethod display-scene ((s lose-scene) window)
  (draw-string window 0 0 "           You lose.")
  (draw-string window 0 1 "-- press [Enter] to restart --"))

(defmethod handle-input ((s lose-scene) key)
  (case key
    (#\Newline (make-instance 'start-scene))
    (otherwise s)))


;;;; drawing --------------------------------------------------------------------

(defun draw-string (window x y format-string &rest args)
  (charms:write-string-at-point window
				(apply #'format nil format-string args)
				x
				y))

;;;; main -----------------------------------------------------------------------

(defun initialize ()
  (setf *running* t)
  (make-instance 'start-scene))

(defun gameloop ()
  (let* ((window charms:*standard-window*)
	 (scene  (initialize)))
    (loop named :game-loop
       while *running*
       do (progn
	    (charms:clear-window window)
	    (display-scene scene window)
	    (setf scene (handle-input scene (charms:get-char window :ignore-error t)))))))

(defun main ()
  (charms:with-curses ()
    (charms:disable-echoing)
    (charms:enable-raw-input :interpret-control-characters t)
    (gameloop)))
