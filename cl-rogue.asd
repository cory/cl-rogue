#|
  This file is a part of cl-rogue project.
  Copyright (c) 2018 Cory Chamblin (c@chambl.in)
|#

#|
  Author: Cory Chamblin (c@chambl.in)
|#

(defsystem "cl-rogue"
  :version "0.1.0"
  :author "Cory Chamblin"
  :license "BSD"
  :depends-on ("cl-charms")
  :components ((:module "src"
                :components
                ((:file "cl-rogue"))))
  :description ""
  :long-description
  #.(read-file-string
     (subpathname *load-pathname* "README.markdown"))
  :in-order-to ((test-op (test-op "cl-rogue-test"))))
