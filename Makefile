LISP ?= sbcl

swank:
	$(LISP) --eval "(ql:quickload '(:swank) :silent t)" \
                --eval "(swank:create-server :port 5555 :dont-close t)" \
                --eval "(loop (sleep 1.0))"

run:
	$(LISP) --eval "(asdf:load-system :cl-rogue)" \
                --eval "(cl-rogue:main)"

