#|
  This file is a part of cl-rogue project.
  Copyright (c) 2018 Cory Chamblin (c@chambl.in)
|#

(defsystem "cl-rogue-test"
  :defsystem-depends-on ("prove-asdf")
  :author "Cory Chamblin"
  :license "BSD"
  :depends-on ("cl-rogue"
               "prove")
  :components ((:module "tests"
                :components
                ((:test-file "cl-rogue"))))
  :description "Test system for cl-rogue"

  :perform (test-op (op c) (symbol-call :prove-asdf :run-test-system c)))
