# A Roguelike in Common Lisp

This is the code for the [A Roguelike in Common Lisp](https://chamblin.net/tag/roguelike/) series.

## Usage

Assuming that `~/quicklisp/local-projects` is in your ASDF search path.

```
cd ~/quicklisp/local-projects/
git clone git@gitlab.com:cory/cl-rogue.git
cd cl-rogue
git checkout part-01
sbcl --eval "(asdf:load-system :cl-rogue)" --eval "(cl-rogue:main)"
```

Each part of the series has a unique branch (e.g. `part-02`) so you can see exactly how I leave the code at the end of each section.

## Author

* Cory Chamblin (c@chambl.in)

## Copyright

Copyright (c) 2018 Cory Chamblin (c@chambl.in)

## License

Licensed under the BSD License.
